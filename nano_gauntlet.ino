#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#define PIN1        6
#define PIN2        7
#define BUTTON_PIN  8
#define NUMPIXELS1  7
#define NUMPIXELS2  8
#define DELAYVAL    100

Adafruit_NeoPixel pixels1(NUMPIXELS1, PIN1, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixels2(NUMPIXELS2, PIN2, NEO_GRB + NEO_KHZ800);

uint32_t white = pixels2.Color(255, 255, 255, 255);

void setup() {
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  pixels1.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  pixels1.clear(); // Set all pixel colors to 'off'

  pixels1.setBrightness(255);

  pixels1.setPixelColor(0, 0, 255, 0, 0);
  pixels1.setPixelColor(1, 0, 0, 255, 0);
  pixels1.setPixelColor(2, 75, 0, 130, 0);
  pixels1.setPixelColor(3, 255, 0, 0, 0);
  pixels1.setPixelColor(4, 255, 140, 0, 0);
  pixels1.setPixelColor(5, 255, 255, 0, 0);
  pixels1.setPixelColor(6, 255, 255, 0, 0);

  pixels1.show();
  
  pixels2.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  pixels2.clear(); // Set all pixel colors to 'off'
  pixels2.show();

  pinMode(BUTTON_PIN, INPUT);
  digitalWrite(BUTTON_PIN, HIGH); // pull-up

  Serial.begin(9600);
}
void loop() {

// handle button
  boolean button_pressed = handle_button();

  // do other things
  Serial.print(button_pressed ? "^" : "-.");
  while(button_pressed){
    Serial.print(button_pressed ? "A" : "B");

    pixels2.setBrightness(50);

    pixels2.fill(white, 0, NUMPIXELS2);
    pixels2.show();

    delay(DELAYVAL);

    button_pressed = handle_button();
  }

  pixels2.clear();
  pixels2.show();
  
}

boolean handle_button()
{
  int button_pressed = !digitalRead(BUTTON_PIN); // pin low -> pressed
  return button_pressed;
}
