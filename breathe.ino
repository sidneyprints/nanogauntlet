#include <Adafruit_NeoPixel.h>

#define PIN1 6
#define PIN2 7
#define BUTTON_PIN 8

Adafruit_NeoPixel strip(7, PIN1, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixels2(8, PIN2, NEO_GRB + NEO_KHZ800);

uint32_t white = pixels2.Color(255, 255, 255, 255);

void setup() {
  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
    clock_prescale_set(clock_div_1);
  #endif

  strip.begin();
  strip.setBrightness(85);  // Lower brightness and save eyeballs!
  strip.show(); // Initialize all pixels to 'off'

  pinMode(BUTTON_PIN, INPUT);
  digitalWrite(BUTTON_PIN, HIGH); // pull-up

  Serial.begin(9600);

}

void loop() {
  float MaximumBrightness = 225;
  float SpeedFactor = 0.008; // I don't actually know what would look good
  float StepDelay = 5; // ms for a step delay on the lights

  for (int i = 0; i < 65535; i++) {
    
    float intensity = MaximumBrightness /2.0 * (1.0 + sin(SpeedFactor * i));
    strip.setBrightness(intensity+30);

    strip.setPixelColor(0, 0, 255, 0, 0);
    strip.setPixelColor(1, 0, 0, 255, 0);
    strip.setPixelColor(2, 75, 0, 130, 0);
    strip.setPixelColor(3, 255, 0, 0, 0);
    strip.setPixelColor(4, 255, 140, 0, 0);
    strip.setPixelColor(5, 255, 255, 0, 0);
    strip.setPixelColor(6, 255, 255, 0, 0);

    strip.show();
    delay(StepDelay);
  }
}

boolean handle_button()
{
  int button_pressed = !digitalRead(BUTTON_PIN); // pin low -> pressed
  return button_pressed;
}
